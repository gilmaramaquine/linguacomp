import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {HabilidadeService} from './../../habilidade/habilidade.service';
import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {Validators, FormBuilder, FormGroup, FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import * as $ from 'jquery';

import {Competencia} from '../../../../core/model/competenciaModel';
import {Conhecimento} from '../../../../core/model/conhecimentoModel';
import {Habilidade} from '../../../../core/model/habilidadeModel';
import {Atitude} from '../../../../core/model/atitudeModel';
import {ConhecimentoService} from '../../conhecimento/conhecimento.service';
import {SelectionModel} from '@angular/cdk/collections';
import {MatTableDataSource} from '@angular/material';
import {AtitudeService} from '../../atitude/atitude.service';
import {CompetenciaService} from '../competencia.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastyService} from 'ng2-toasty';
import {JwtHelperService} from '@auth0/angular-jwt';

@Component({
  selector: 'app-competencia-novo',
  templateUrl: './competencia-novo.component.html',
  styleUrls: ['./competencia-novo.component.scss']
})
export class CompetenciaNovoComponent implements OnInit {

  token = localStorage.getItem('token');
  competencia = new Competencia();
  panelOpenState = false;

  conhecimentos = new MatTableDataSource<Conhecimento>();
  habilidades = new MatTableDataSource<Habilidade>();
  atitudes = new MatTableDataSource<Atitude>();

  conhecimentosSelecionados = new Array<Conhecimento>();

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  displayedColumns: string[] = ['select', 'descricao'];
  selection = new SelectionModel<number>(true, []);
  selection2 = new SelectionModel<number>(true, []);
  selection3 = new SelectionModel<number>(true, []);

  jwt = new JwtHelperService();

  loading = false;
  id: any;
  retorno: any;


  constructor(
    private _formBuilder: FormBuilder,
    private conhecimentoService: ConhecimentoService,
    private habilidadeService: HabilidadeService,
    private atitudeService: AtitudeService,
    private competenciaService: CompetenciaService,
    private router: Router,
    private route: ActivatedRoute,
    private toasty: ToastyService
  ) {
  }

  ngOnInit() {

    this.id = this.route.snapshot.params['id'];
    if (this.id) {
      this.getCompetencia(this.id);
    }

    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });

    this.listarConhecimentos();
    this.listarHabilidades();
    this.listarAtitudes();
  }

  listarConhecimentos() {
    this.loading = true;
    this.conhecimentoService.listar()
      .subscribe(conhecimentos => {
        console.log('conhecimentos: ', conhecimentos);
        this.conhecimentos = new MatTableDataSource<Conhecimento>(conhecimentos);
        this.loading = false;
      });
  }

  listarHabilidades() {
    this.loading = true;
    this.habilidadeService.listar()
      .subscribe(habilidades => {
        console.log('habilidades: ', habilidades);
        this.habilidades = new MatTableDataSource<Habilidade>(habilidades);
        this.loading = false;
      });
  }

  listarAtitudes() {
    this.loading = true;
    this.atitudeService.listar()
      .subscribe(atitudes => {
        console.log('atitudes: ', atitudes);
        this.atitudes = new MatTableDataSource<Atitude>(atitudes);
        this.loading = false;
      });
  }

  isAllSelected() {
    return this.conhecimentos.data.every(row => this.selection.isSelected(row.id));
  }

  masterToggle() {

    if (this.isAllSelected()) {
      this.conhecimentos.data.forEach(row => this.selection.deselect(row.id));
    } else {
      this.conhecimentos.data.forEach(row => this.selection.select(row.id));
    }
  }

  checkboxLabel(row?: Conhecimento): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row.id) ? 'deselect' : 'select'} row ${row.descricao + 1}`;
  }

  isAllSelected2() {
    const numSelected = this.selection2.selected.length;
    const numRows = this.habilidades.data.length;
    return numSelected === numRows;
  }

  masterToggle2() {
    this.isAllSelected2() ?
      this.selection2.clear() :
      this.habilidades.data.forEach(row => this.selection2.select(row.id));
  }

  checkboxLabel2(row?: Habilidade): string {
    if (!row) {
      return `${this.isAllSelected2() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection2.isSelected(row.id) ? 'deselect' : 'select'} row ${row.descricao + 1}`;
  }

  isAllSelected3() {
    const numSelected = this.selection3.selected.length;
    const numRows = this.atitudes.data.length;
    return numSelected === numRows;
  }

  masterToggle3() {
    this.isAllSelected3() ?
      this.selection3.clear() :
      this.atitudes.data.forEach(row => this.selection3.select(row.id));
  }

  checkboxLabel3(row?: Atitude): string {
    if (!row) {
      return `${this.isAllSelected3() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection3.isSelected(row.id) ? 'deselect' : 'select'} row ${row.descricao + 1}`;
  }

  getCompetencia(id: number) {
    this.loading = true;
    this.competenciaService.getCompetencia(id)
      .subscribe(competencia => {
        this.loading = false;
        this.competencia = competencia;

        competencia.conhecimentos.forEach(value => {
          this.selection.toggle(value.id);
        });

        competencia.habilidades.forEach(value => {
          this.selection2.toggle(value.id);
        });

        competencia.atitudes.forEach(value => {
          this.selection3.toggle(value.id);
        });
      });
  }

  getDescricaoConhecimento(id: number) {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let texto;
    this.conhecimentos.data.forEach(value => {
      if (value.id === id) {
        texto = value.descricao;
      }
    });
    return texto;
  }

  getDescricaoHabilidade(id: number) {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let texto;
    this.habilidades.data.forEach(value => {
      if (value.id === id) {
        texto = value.descricao;
      }
    });
    return texto;
  }

  getDescricaoAtitudes(id: number) {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let texto;
    this.atitudes.data.forEach(value => {
      if (value.id === id) {
        texto = value.descricao;
      }
    });
    return texto;
  }

  retornaConhecimentosSelecionados() {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let list = [];
    this.selection.selected.forEach(value => {
      this.conhecimentos.data.forEach(value2 => {
        if (value2.id === value) {
          list.push(value2);
        }
      });
    });
    return list;
  }

  retornaHabilidadesSelecionadas() {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let list = [];
    this.selection2.selected.forEach(value => {
      this.habilidades.data.forEach(value2 => {
        if (value2.id === value) {
          list.push(value2);
        }
      });
    });
    return list;
  }

  retornaAtituddesSelecionadas() {
    // tslint:disable-next-line:label-position no-unused-expression prefer-const
    let list = [];
    this.selection3.selected.forEach(value => {
      this.atitudes.data.forEach(value2 => {
        if (value2.id === value) {
          list.push(value2);
        }
      });
    });
    return list;
  }

  salvar() {
    this.competencia.conhecimentos = this.retornaConhecimentosSelecionados();
    this.competencia.habilidades = this.retornaHabilidadesSelecionadas();
    this.competencia.atitudes = this.retornaAtituddesSelecionadas();
    this.competencia.professor = {'id': this.jwt.decodeToken(this.token).id};

    this.loading = true;
    if (this.competencia.id) {
      this.competenciaService.editar(this.competencia)
        .subscribe(
          info => {
            console.log('competencia: ', this.competencia);
            this.router.navigateByUrl('/competencia');
            this.toasty.success('Cadastrado com sucesso!');
            this.loading = false;
          }, error => {
            this.toasty.error('Não foi possível cadastrar!');
            this.loading = false;
          });
    } else {
      this.competenciaService.salvar(this.competencia)
        .subscribe(
          info => {
            console.log('competencia: ', this.competencia);
            this.router.navigateByUrl('/competencia');
            this.toasty.success('Cadastrado com sucesso!');
            this.loading = false;
          }, error => {
            this.toasty.error('Não foi possível cadastrar!');
            this.loading = false;
          });
    }
  }

}
